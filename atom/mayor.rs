fn main() {
  let a = 5;
  let b = -3;

  println!("Primer numero {}", a);
  if a < 0 {
    println!("{} es negativo", a);
  } else if a > 0 {
    println!("{} es positivo", a);
  }
  if b < 0 {
    println!("{} es negativo", b);
  } else if b > 0 {
    println!("{} es positivo", b);
  }
}
